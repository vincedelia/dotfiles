set shell=/bin/bash

set nocp

let os = substitute(system('uname'), "\n", "", "")

" Use pathogen to easily modify the runtime path to include all
" " plugins under the ~/.vim/bundle directory
call pathogen#helptags()
call pathogen#runtime_append_all_bundles()

filetype off
syntax on
filetype plugin indent on

set et
set sw=4
set smarttab
map <f2> :w\|!python %
set sts=4

" highlight in red column 81
set colorcolumn=81

exec "set listchars=tab:\uBB\uBB,trail:\uB7,nbsp:~"
set list

if os == "Darwin"
    nnoremap , :
    nnoremap : ,
else
    nnoremap ; :
    nnoremap : ;
endif


" spell checking
set spell spelllang=en_us
hi clear SpellBad
hi SpellBad cterm=underline

" search options
set incsearch "Lookahead as search pattern is specified
set ignorecase "Ignore case in all searches...
set smartcase "...unless uppercase letters used
set hlsearch "Highlight all matches

" bigger comments
highlight Comment term=bold  ctermfg=white
hi Search ctermbg=LightBlue

set number

" highlight for visual modes
hi Visual  term=reverse cterm=reverse ctermbg=LightBlue


" automatically source ~/.vimrc when it changes
augroup myvimrc
    au!
    autocmd bufwritepost .vimrc source ~/.vimrc
augroup END

" status line
set statusline=%n\)\ %F%m%r%h%w\ [TYPE=%Y]\ [POS=%04l,%04v]\ [%p%%]\ [LEN=%L]
set laststatus=2


au BufNewFile,BufRead *.app set filetype=python

if filereadable("/usr/lib/llvm-3.2/lib/libclang.so")
    let g:clang_library_path="/usr/lib/llvm-3.2/lib/"
else
    let g:clang_library_path="/usr/lib/"
endif

" Directory colors
highlight Directory guifg=#FF0000 ctermfg=red

