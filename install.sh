#!/bin/bash

# symlinks all the .stuff here in the home. If the file already exists, move
# the old to .olddotstuff

SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )" 
DOTFILES=$(ls -a $SCRIPTDIR | grep '^\.[a-zA-Z]\+' | grep -v '.*.swp$' | grep -v '.git*')
ARCHIVEDIR=$HOME/.olddotstuff/$(date +%s)

function archive() {
    mkdir -p $ARCHIVEDIR
    echo mv $1 $ARCHIVEDIR
}

os_version=$(uname)
if [ $os_version == "Darwin" ]
then
    brew install llvm --with-clang --with-asan
else
    sudo apt-get update
    sudo apt-get install clang libclang-dev
    sudo apt-get install python-pip
fi
sudo pip install jedi

for dotfile in $DOTFILES
do
    destination=$HOME/$dotfile
    source=$SCRIPTDIR/$dotfile

    # in general archive
    if [ -e $destination ]
    then
        archive $destination
    fi

    # if it is a symlink we remove it
    if [ -h $destination ]
    then
        rm $destination
    fi
    ln -s $source $destination
done
