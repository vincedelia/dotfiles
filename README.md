# Introduction
This is the collection of the settings and vim plugins I use.

# Install
Don't forget to initialize the git submodules first!
    $ git submodule init
    $ git submodule update

Then, run the install script
    $ ./install.sh


